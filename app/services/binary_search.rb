# frozen_string_literal: true

class BinarySearch

  def self.search(arr, target)
    left = 0
    right = arr.size - 1

    while left <= right
      mid = (left + right) / 2

      return true if arr[mid] == target
      # if result is less than 0
      # ignore left half
      if arr[mid] < target
        # set the left most value to the mid point + 1
        left = mid + 1
      # if result is greater than 0
      # ignore right half
      else
        # set the right most point to the mid point - 1
        right = mid - 1
      end
    end

    false
  end
end