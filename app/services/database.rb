# frozen_string_literal: true

# Class for all database interactions
class Database
  DB_FILE ||= "#{Rails.root}/db/database.json"

  def self.save(user)
    password = user.delete(:password)
    user.delete(:password_confirmation)
    user[:username] = set_username(user[:username])
    user.merge!(PasswordHash.hash(password))
    add_to_file(user)
    user
  end

  def self.get(username)
    return false unless check_username(username)
    grab_user(username)
  end

  def self.user_list
    users
  end

  private_class_method

  def self.set_username(username)
    user_arr = users
    exists = BinarySearch.search user_arr, username
    if exists
      username = username.gsub(/(_\d+)/, '')
      count = user_arr.count { |name| /(#{username})/ =~ name }
      username += "_#{count}"
    end
    username
  end

  def self.check_username(username)
    user_arr = users
    BinarySearch.search user_arr, username
  end

  def self.grab_user(username)
    data = read_file
    data.shift
    data.map! { |d| JSON.parse(d) }
    data.select { |d| d['username'] == username }.first
  end

  def self.add_to_file(user)
    data = read_file
    users_arr = JSON.parse(data.shift)
    users_arr << user[:username]
    data << user.to_json

    File.open(DB_FILE, 'w') do |file|
      file.puts(users_arr.sort.to_json)
      file.puts(data.join("\n"))
    end
  end

  def self.read_file
    File.read(DB_FILE).split("\n")
  end

  def self.users
    JSON.parse(File.open(DB_FILE, &:readline))
  end
end