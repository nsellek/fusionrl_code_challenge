# frozen_string_literal: true

class SessionsController < ApplicationController

  def new; end

  def create
    user = User.get(params[:username])

    if user && PasswordHash.authenticate(params[:password], user['password_digest'], user['salt'])
      session[:username] = user['username']
      redirect_to dashboard_index_path
    else
      redirect_to login_path(errors: ['Username or Password is invalid'])
    end
  end

  def destroy
    session[:username] = nil
    redirect_to login_path
  end
end