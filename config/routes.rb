Rails.application.routes.draw do
  root 'sessions#new'
  get '/signup' => 'users#new'
  post '/users' => 'users#create'

  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'

  resources :dashboard, only: [:index] do
    collection do
      get :user_list
    end
  end
end
