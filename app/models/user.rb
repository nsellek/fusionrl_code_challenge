# frozen_string_literal: true

class User < ApplicationRecord

  def self.valid_user?(user)
    return { errors: 'Passwords must match' } unless check_password(user)
    user
  end

  def self.persist(user)
    Database.save(user)
  end

  def self.get(username)
    Database.get(username)
  end

  def self.list
    Database.user_list
  end

  private_class_method

  def self.check_password(user)
    user[:password] == user[:password_confirmation]
  end
end