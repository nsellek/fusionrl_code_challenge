# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :authorize

  def index; end

  def user_list
    @user_list = User.list
  end
end