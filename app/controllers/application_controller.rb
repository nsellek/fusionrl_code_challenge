class ApplicationController < ActionController::Base

  def current_user
    @current_user ||= User.get(session[:username]) if session[:username]
  end

  helper_method :current_user

  def authorize
    redirect_to login_path(errors: ['Please log in']) unless current_user
  end
end
