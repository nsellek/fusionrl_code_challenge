# frozen_string_literal: true

# class for hashing passwords with salt
class PasswordHash
  def self.hash(pass)
    pass_salt = salt
    salted = Digest::SHA256.hexdigest(pass + pass_salt)
    { password_digest: salted, salt: pass_salt }
  end

  def self.authenticate(clean_pass, hashed_pass, salt)
    salted = Digest::SHA256.hexdigest(clean_pass + salt)

    hashed_pass == salted
  end

  private_class_method

  def self.salt
    o = [('a'..'z'), ('A'..'Z'), ('0'..'9')].map(&:to_a).flatten
    (0...20).map { o[rand(o.length)] }.join
  end
end
