# frozen_string_literal: true

class UsersController < ApplicationController

  def new; end

  def create
    user = User.valid_user?(user_params)

    if user.key?(:errors)
      redirect_to signup_path(errors: user[:errors])
    else
      user = User.persist(user_params)
      session[:username] = user['username']
      redirect_to dashboard_index_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :password, :password_confirmation)
  end
end